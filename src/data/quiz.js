const quiz = {
    id: 'some-id-123',
    code: 'VLN_3',
    title: 'Sample quiz',
    description: 'i have no idea what i am doing',
    active: true,
    questions: [
        {
            id: 1,
            text: 'How are you?',
            type: 'RADIO',
            answers: [
                {
                    id: 1,
                    text: 'yes',
                },
                {
                    id: 2,
                    text: 'left',
                },
                {
                    id: 3,
                    text: 'red',
                },
                {
                    id: 4,
                    text: '12',
                },
            ],
            correctAnswer: '3'
        },
        {
            id: 2,
            text: 'Why?',
            type: 'LONG',
            correctAnswer: 'no reason'
        },
        {
            id: 3,
            text: 'Favorite animal?',
            type: 'SHORT',
            correctAnswer: 'cat'
        },
        {
            id: 4,
            text: 'Which options you preder',
            type: 'CHECK',
            answers: [
                {
                    id: 1,
                    text: 'Option 1',
                },
                {
                    id: 2,
                    text: 'Option 2',
                },
                {
                    id: 3,
                    text: 'Option 3',
                },
                {
                    id: 4,
                    text: 'Option 4',
                },
            ],
            correctAnswer: ['Option 1', 'Option 4']
        },
    ],
};

export default quiz;
