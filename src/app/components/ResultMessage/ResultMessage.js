import React from 'react';

const resultMessage = ( props ) => (
    <p>Correct answers: {props.correctAnswersCounter}</p> 
);

export default resultMessage;