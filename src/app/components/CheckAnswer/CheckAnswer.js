import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox/Checkbox';
import FormGroup from '@material-ui/core/FormGroup/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';

class CheckAnswer extends React.Component {
    /* eslint-disable react/sort-comp */
    answers;
    isAnswerCorrect;
    /* eslint-enable */

    state = {
        isValid: false,
        touched: false
    }

    componentDidMount = () => {
        this.answers = {};
        this.isAnswerCorrect = {};
    };

    handleChange = (e, isChecked) => {
        if (!this.state.touched) {
            this.setState({touched: true});
        }
        const { value } = e.target;
        let answersArray = [];

        if (isChecked) {
            this.answers[value] = true;
        } else {
            delete this.answers[value];
        }

        for (var key in this.answers) {
            if (this.answers[key]) {
                answersArray.push(key);
            }
        }

        if(answersArray.length >= 2) {
            this.setState({isValid: true});
        }
        else {
            this.setState({isValid: false});
        }

        if (JSON.stringify(answersArray) === JSON.stringify(this.props.correctAnswer)) {
            this.props.onChange(Object.keys(this.answers), true);
        }
        else {
            this.props.onChange(Object.keys(this.answers), false);
        }
    };

    render() {
        return (
            <FormGroup>
                {this.props.answers.map(a => (
                    <FormControlLabel
                        key={a.id}
                        control={(
                            <Checkbox
                                onChange={this.handleChange}
                                value={a.text}
                            />
                        )}
                        label={a.text}
                    />
                ))}
                {!this.state.isValid && this.state.touched && (
                    <p>Two or more answers must be selected</p>
                        
                )}
            
            </FormGroup>
        );
    }
}

CheckAnswer.propTypes = {
    answers: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        text: PropTypes.string,
    })).isRequired,
    onChange: PropTypes.func.isRequired,
};

export default CheckAnswer;
