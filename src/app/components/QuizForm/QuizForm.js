import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button/Button';

import Header from 'components/Header/Header';
import Question from 'components/Question/Question';
import ResultMessage from '../ResultMessage/ResultMessage';

import './quizForm.scss';
import Typography from '@material-ui/core/Typography/Typography';

class QuizForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            formValid: true,
            correctAnswersCounter: 0,
            formSubbitted: false
        };
    }
    /* eslint-disable react/sort-comp */
    feedback;
    /* eslint-enable */

    componentDidMount = () => {
        this.feedback = {
            quizId: this.props.quiz.id,
            answers: {},
            correctAnswers: {},
            
        };
    };

    handleChange = (id, value, ifCorrect) => {
        this.feedback.answers[`question-${id}`] = value;

        if (ifCorrect) {
            this.feedback.correctAnswers[id] = true;
        } else {
            this.feedback.correctAnswers[id] = false;
        }
    };

    isFormValid = () => {
        const { quiz } = this.props;
        const { answers } = this.feedback;
        // console.log('quiz.questions', quiz.questions);
        // console.log('answers', answers);
        // console.log('answers.length', Object.keys(answers).length);
        return quiz.questions.length === Object.keys(answers).length;
    }

    handleSubmit = (e) => {
        
        e.preventDefault();
        const isValid = this.isFormValid();
        if (isValid) {
            this.props.submitForm(this.feedback);
           
        }
        this.setState({ formValid: isValid, formSubbitted: true});
        
        this.setState({correctAnswersCounter: 0});
        
        for (var key in this.feedback.correctAnswers) {
            if (this.feedback.correctAnswers[key]) {
                let counter = this.state.correctAnswersCounter+1;
                this.setState({correctAnswersCounter: counter})
            }
        }
    };

    render() {
        const { isLoading, error, quiz } = this.props;
        let score = null;
        if (this.state.formSubbitted && this.state.formValid) {
            score = <ResultMessage correctAnswersCounter={this.state.correctAnswersCounter}/> 
        }

        return (
            <form className="quiz-form" noValidate onSubmit={this.handleSubmit}>
                <Header text={quiz.title} />
                {error && (
                    <Typography className="description" variant="h5" color="error">
                        {error}
                    </Typography>
                )}
                {!this.state.formValid && (
                    <Typography className="description" variant="h5" color="error">
                        Not all questions have been answered
                    </Typography>
                )}
                <Typography className="description" variant="h5" color="inherit">
                    {quiz.description}
                </Typography>
                {quiz.questions && quiz.questions.map(q => (
                    <Question
                        key={q.id}
                        data={q}
                        onChange={(value, ifCorrect) => this.handleChange(q.id, value, ifCorrect)}
                    />
                ))}
                <div className="control-section">
                    <Button
                        disabled={isLoading || !(quiz.id)}
                        variant="contained"
                        color="primary"
                        onClick={this.handleSubmit}
                        type="submit"
                    >
                        {isLoading ? 'Saving...' : 'Submit'}
                    </Button>
                    <Button variant="contained" type="button">
                        Cancel
                    </Button>
                </div>
                {score}
            </form>
        );
    }
}

QuizForm.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    quiz: PropTypes.shape({
        id: PropTypes.string,
        code: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        active: PropTypes.bool,
        questions: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            text: PropTypes.string,
            type: PropTypes.string,
            answers: PropTypes.arrayOf(PropTypes.shape({
                id: PropTypes.number,
                text: PropTypes.string,
            })),
        })),
    }).isRequired,
    submitForm: PropTypes.func.isRequired,
};

export default QuizForm;
